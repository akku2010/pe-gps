import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import "rxjs/add/operator/timeout";
import { LoadingController, Events } from 'ionic-angular';
declare var google;

@Injectable()
export class ApiCallerProvider {
  token: any;
  // service = new google.maps.DistanceMatrixService();
  private headers = new Headers();
  link: string = "http://139.59.50.225/";
  loading: any;
  headnew: Headers;
  travelDetailsObject: any = {};
  constructor(public http: Http, public loadingCtrl: LoadingController, public events: Events) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    if (this.token == undefined) {
      if (localStorage.getItem("AuthToken") != null) {
        this.token = localStorage.getItem("AuthToken");
        this.headers.append('Authorization', this.token);
      }
    }
  }



  // getTravelDetails(ori, dest) {

  // this.service.getDistanceMatrix({
  //     origins: [new google.maps.LatLng(ori.lat, ori.lng)],
  //     destinations: [new google.maps.LatLng(dest.lat, dest.lng)],
  //     travelMode: 'DRIVING'
  //   }, this.callback);
  // }

  // callback(response, status) {
  //   // debugger
  //   let travelDetailsObject;
  //   if (status == 'OK') {
  //     var origins = response.originAddresses;
  //     var destinations = response.destinationAddresses;
  //     for (var i = 0; i < origins.length; i++) {
  //       var results = response.rows[i].elements;
  //       for (var j = 0; j < results.length; j++) {
  //         var element = results[j];
  //         var distance = element.distance.text;
  //         var duration = element.duration.text;
  //         var from = origins[i];
  //         var to = destinations[j];
  //         travelDetailsObject = {
  //           distance: distance,
  //           duration: duration
  //         }
  //       }
  //     }
  //     this.travelDetailsObject = travelDetailsObject;
  //     console.log("distance matrix: ", travelDetailsObject)
  //   }
  // }


  startLoading() {
    return this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
  }

  stopLoading() {
    return this.loading.dismiss();
  }

  getdevicesApi(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getPoiCall(_id) {
    // 5b0bfedc43b09153a28c0b65
    return this.http.get("https://www.oneqlik.in/trackroute/sbUsers/routePoi?user=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  loginCall(data) {
    return this.http.post(this.link + "users/LoginWithOtp", data, { headers: this.headers })
      .map(res => res.json());
  }

  getDeviceByUserCall(userdetails) {
    return this.http.get("http://13.126.36.205:3000/devices/getDeviceByUser?id=" + userdetails._id + "&email=" + userdetails.email, { headers: this.headers })
      .map(res => res.json());
  }

  getRecentNotifCall(_id) {
    return this.http.get(this.link + 'notifs/getRecent?user=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  pushnotifyCall(pushdata) {
    return this.http.post(this.link + "users/PushNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }

  getSchedules(_id) {
    this.token = undefined;
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    if (localStorage.getItem("AuthToken") != null) {
      this.token = localStorage.getItem("AuthToken");
      console.log("Headers token => ", this.token)
      this.headers.append('Authorization', this.token);
    }
    console.log("headers=> " + JSON.stringify(this.headers))
    return this.http.get(this.link + "student/parentScheduled?id=" + _id, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  tripStatusCall(data) {
    // debugger
    this.token = undefined;
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    if (localStorage.getItem("AuthToken") != null) {
      this.token = localStorage.getItem("AuthToken");
      console.log("Headers token => ", this.token)
      this.headers.append('Authorization', this.token);
    }
    // this.headnew = new Headers({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': this.token });
    return this.http.post(this.link + 'schoolTrip/find', data, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  stoppage_detail(_id, from, to, device_id) {
    return this.http.get(this.link + 'stoppage/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  gpsCall(device_id, from, to) {
    return this.http.get(this.link + 'gps/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  getDistanceSpeedCall(device_id, from, to) {
    return this.http.get(this.link + 'gps/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  // Rahul update service/////
  getStudent(data) {

    return this.http.post("http://139.59.50.225/student/find", data, { headers: this.headers })
      .map(res => res.json());

  }

  studentlistreportCall(data) {

    return this.http.post("http://139.59.50.225/attendance/studentList", data, { headers: this.headers })
      .map(res => res.json());

  }

  ReportCall(data) {

    // return this.http.post("http://www.thingslab.in/attendance/studentDayWiseReport", data, { headers: this.headers })
    return this.http.post("http://139.59.50.225/attendance/studentDayWiseReport1", data, { headers: this.headers })
      .map(res => res.json());

  }


  studenEdit(data) {

    return this.http.post("http://139.59.50.225/student/edit", data, { headers: this.headers })
      .map(res => res.json());

  }

  getprofileApi(_id) {
    return this.http.get("http://139.59.50.225/users/parentData?id=" + _id, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }
  getProfilestudentApi(_id) {
    return this.http.get("http://139.59.50.225/users/parentData?id=" + _id, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }

  shareLivetrackCall(data) {
    return this.http.post("http://139.59.50.225/share", data, { headers: this.headers })
      .map(res => res.json());
  }

  getTrackrouteCall(id, user) {
    // 5b0bfedc43b09153a28c0b65
    return this.http.get("http://139.59.50.225/trackRoute/routepath/getRoutePathWithPoi?id=" + id + '&user=' + user, { headers: this.headers })
      .map(res => res.json());
  }

  pullnotifyCall(pushdata) {
    return this.http.post("http://139.59.50.225/users/PullNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }
}

