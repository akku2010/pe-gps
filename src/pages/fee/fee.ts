import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
declare var RazorpayCheckout: any;
@IonicPage()
@Component({
  selector: 'page-fee',
  templateUrl: 'fee.html',
})
export class FeePage {
  index: number = 0;
  pendingList: any = [
    {
      id: this.index + 1,
      title: "January",
      date: new Date().toISOString(),
      status: "Paid",
      amount: "1200"
    },
    {
      id: this.index + 1,
      title: "February",
      date: new Date().toISOString(),
      status: "Paid",
      amount: "1200"
    },
    {
      id: this.index + 1,
      title: "March",
      date: new Date().toISOString(),
      status: "Paid",
      amount: "1200"
    },
    {
      id: this.index + 1,
      title: "April",
      date: null,
      status: "Pending",
      amount: null
    }
  ];
  islogin: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.islogin = JSON.parse(localStorage.getItem('details'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeePage');
  }
  razor_key: string = 'rzp_live_jB4onRx1BUUvxt';
  paymentAmount: number = 120000;
  currency: any = 'INR';
  paynow() {
    // payWithRazor() {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/GO0jiDP.jpg',
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
      amount: this.paymentAmount, // Payment amount in smallest denomiation e.g. cents for USD
      name: "xyz",
      prefill: {
        email: this.islogin.email,
        contact: this.islogin.phn,
        name: this.islogin.fn + ' ' + this.islogin.ln
      },
      theme: {
        color: '#ef4138'
      },
      modal: {
        ondismiss: function () {
          console.log('dismissed')
        }
      }
    };

    var successCallback = function (payment_id) {
      alert('payment_id: ' + payment_id);
      this.updateExpDate();
    };

    var cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
    // }
  }

}
