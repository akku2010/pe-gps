import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Events } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit, OnDestroy {
  loginDetails: any = {};
  schedules: any[] = [];
  token: any;
  pushnotify: any;
  connected: Subscription;
  disconnected: Subscription;
  inetrvalId: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpUrl: ApiCallerProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public storage: Storage,
    private network: Network,
    private push: Push,
    private events: Events
  ) {
    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
    ///////////////////////////////
    this.events.publish('user:updated', this.loginDetails);
    /////////////////////////////////
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ionViewDidEnter() {

    this.connected = this.network.onConnect().subscribe(data => {
      this.getSchedules();
    }, error => console.log(error));

    this.disconnected = this.network.onDisconnect().subscribe(data => {
      this.displayNetworkUpdate(data.type);
    }, error => console.log(error));
  }

  ionViewWillLeave() {
    this.connected.unsubscribe();
    this.disconnected.unsubscribe();
    debugger
    if (this.inetrvalId) {
      console.log("interval id cleared")
      // this.inetrvalId.clear();
      clearInterval(this.inetrvalId);

    }
  }

  displayNetworkUpdate(connectionState: string) {
    // let networkType = this.network.type;
    this.toastCtrl.create({
      // message: `You are now ${connectionState} via ${networkType}`,
      message: `You are now ${connectionState}`,
      duration: 3000,
      position: 'bottom'
    }).present();
  }

  ngOnInit() {
    this.getSchedules();
    this.inetrvalId = setInterval(() => {
      this.getSchedules();
    }, 10000);
    console.log("interval: ", this.inetrvalId);
  }

  ngOnDestroy() {
    localStorage.removeItem("loader-Loaded");
    // localStorage.removeItem("loader-Loaded")
    // if (this.inetrvalId) {

    //   this.inetrvalId.clear();
    // }
  }

  getSchedules() {
    // debugger
    if (localStorage.getItem("loader-Loaded") === null) {
      this.httpUrl.startLoading().present();
    }

    this.httpUrl.getSchedules(this.loginDetails._id)
      .subscribe(data => {
        if (localStorage.getItem("loader-Loaded") === null) {
          localStorage.setItem("loader-Loaded", "true");
          this.httpUrl.stopLoading();
        }

        console.log("data=> ", data);
        if (data.length > 0) {
          this.innerFunc(data);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Schedules not found..',
            duration: 1500,
            position: 'middle'
          })
          toast.present();
        }
        this.addPushNotify();
      },
        err => {
          if (localStorage.getItem("loader-Loaded") === null) {
            this.httpUrl.stopLoading();
          }
          // this.httpUrl.stopLoading();
          console.log("error got=> ", err)
        });
  }

  innerFunc(sched) {
    let that = this;
    that.schedules = [];
    var i = 0, howManyTimes = sched.length;
    function f() {
      that.schedules.push({
        'scheduleschools': sched[i].scheduleschools,
        '_id': sched[i]._id
      });
      if (sched[i]._id != null && sched[i]._id != undefined) {
        that.getTripStat(sched[i]);

      } else {
        that.schedules[that.schedules.length - 1].tripStat = 'N/A'
      }
      console.log("finally data: " + that.schedules);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 200);
      }
    } f();
  }

  getTripStat(data) {
    let that = this;
    var postData = {
      "_find": {
        "createdAt": {
          "$gte": {
            "_eval": "dayStart",
            "value": new Date().toISOString()
          }
        },
        "ScheduleSchool": {
          "_eval": "Id",
          "value": data._id
        }
      }
    }

    that.httpUrl.tripStatusCall(postData)
      .subscribe(respData => {
        // that.httpUrl.stopLoading();
        console.log('response data', respData)
        if (respData.length == 0) {
          that.schedules[that.schedules.length - 1].tripStat = 'Trip not started yet';
        } else {
          if (respData[0].pickup == "START") {
            that.schedules[that.schedules.length - 1].tripStat = 'Pickup Ongoing';
          } else {
            if (respData[0].drop == "START") {
              that.schedules[that.schedules.length - 1].tripStat = 'Drop Ongoing';
            } else {
              if (respData[0].pickup == "END") {
                that.schedules[that.schedules.length - 1].tripStat = 'Pickup Completed';
              } else {
                if (respData[0].drop == "END") {
                  that.schedules[that.schedules.length - 1].tripStat = 'Drop Completed';
                }
              }
            }
          }
        }
      },
        err => {
          // that.httpUrl.stopLoading();
        });

  }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "Server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: 'Okay',
        handler: () => {
          this.navCtrl.setRoot("TabsPage");
        }
      }]
    });
    alerttemp.present();
  }

  doRefresh(refresher) {
    // console.log('Begin async operation', refresher);
    this.getSchedules();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 200);
  }

  pushSetup() {
    // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
    this.push.createChannel({
      id: "bus_reached",
      description: "bus_reached description",
      sound: 'bus_reached',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('bus_reached Channel created'));

    this.push.createChannel({
      id: "student_deboarded",
      description: "student_deboarded description",
      sound: 'student_deboarded',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('student_deboarded Channel created'));

    this.push.createChannel({
      id: "student_onborded",
      description: "student_onborded description",
      sound: 'student_onborded',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('student_onborded Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('default Channel created'));

    // Delete a channel (Android O and above)
    // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))

    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        icon: 'safety365',
        iconColor: 'red'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {
      // if (localStorage.getItem("notifValue") != null) {
      //   if (localStorage.getItem("notifValue") == 'true') {
      // this.tts.speak(notification.message)
      //   .then(() => console.log('Success'))
      //   .catch((reason: any) => console.log(reason));
      //   }
      // }
      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      } else {
        // this.nav.setRoot('AllNotificationsPage');
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        // alert(registration.registrationId)
        console.log("device token => " + registration.registrationId);
        // console.log("reg type=> " + registration.registrationType);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        // that.storage.set("DEVICE_TOKEN", registration.registrationId);

      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
      // alert('Error with Push plugin' + error)
    });
  }
  // pushSetup() {
  //   // to initialize push notifications
  //   let that = this;
  //   const options: PushOptions = {
  //     android: {
  //       senderID: '644983599736',
  //       icon: 'safety365',
  //       iconColor: 'red'
  //     },
  //     ios: {
  //       alert: 'true',
  //       badge: true,
  //       sound: 'true'
  //     },
  //     windows: {},
  //     browser: {
  //       pushServiceURL: 'http://push.api.phonegap.com/v1/push'
  //     }
  //   };

  //   const pushObject: PushObject = that.push.init(options);

  //   pushObject.on('registration')
  //     .subscribe((registration: any) => {
  //       console.log("device token => " + registration.registrationId);
  //       localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
  //       this.storage.set("DEVICE_TOKEN", registration.registrationId);
  //       this.addPushNotify();
  //     });

  //   pushObject.on('error').subscribe(error => {
  //     console.error('Error with Push plugin', error)
  //   });
  // }

  addPushNotify() {
    // console.log("push notify click")
    this.storage.get("DEVICE_TOKEN").then((res) => {
      if (res) {
        console.log("device token=> " + JSON.stringify(res));
        this.token = res;
        var pushdata = {
          "uid": this.loginDetails._id,
          "token": this.token,
          "os": "android",
          "appName": "Safety365"
        }
        // console.log("pushdata=> " + JSON.stringify(pushdata));
        this.httpUrl.pushnotifyCall(pushdata)
          .subscribe(data => {
            this.pushnotify = data.message;
            console.log("data of push=> " + JSON.stringify(data));
          },
            error => {
              console.log(error);
            });
      } else {
        this.pushSetup();
      }
    });

    // debugger;
  }

  showTrip(data) {
    console.log('show Trip', data)

    var postData = {
      "_find": {
        "createdAt": {
          "$gte": {
            "_eval": "dayStart",
            "value": new Date().toISOString()
          }
        },
        "ScheduleSchool": {
          "_eval": "Id",
          "value": data._id
        }
      }
    }
    this.httpUrl.startLoading().present();
    this.httpUrl.tripStatusCall(postData)
      .subscribe(respData => {

        console.log('response data', respData)
        this.httpUrl.stopLoading();
        if (respData.length == 0) {
          const toast = this.toastCtrl.create({
            message: 'This trip is yet to start.. Please check afetr some time. Thank you!',
            duration: 3000,
            position: 'bottom'
          })
          toast.present();
        } else {
          if (respData[0].pickup == "START" || respData[0].drop == "START") {
            this.navCtrl.push("LiveSingleDevice", {
              imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
              param: respData[0],
              data: data
            });
          } else {
            if (respData[0].pickup == "END" || respData[0].drop == "END") {
              this.navCtrl.push("HistoryMapPage", {
                imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
                param: respData[0]
              });
            }
          }
        }
      },
        err => {
          this.httpUrl.stopLoading();
          console.log("error occured=> ", err)
        });
  }

}
