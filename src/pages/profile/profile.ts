import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, ToastController } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { GoogleMap, GoogleMaps, LatLng, GoogleMapsEvent, Geocoder, GeocoderResult } from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {
  map: GoogleMap;
  profilesStudent: any[] = [];
  profilesParent: any;
  loginDetails: any;
  token: string;
  deviceUUID: string;
  profilesParentname: any;
  profilesParentphone: any;
  profilesParentemail: any;
  // location: any;
  mapElement: any;
  profilesParenterelation: any;
  allData: any = {};
  mapid: any;
  markerlatlong: any;
  // data: { "_id": any; "latLng": { "lat": any; "lng": any; }; };
  lattitude: string;
  longtitude: string;
  studentid: any;
  // stdn: { "_id": any; "latLng": { "lat": string; "lng": string; }; };
  address_show: any;
  locationShow: any;
  addressofstudent: any;
  schoolmapid: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apicall: ApiCallerProvider, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
    console.log("login _id=> " + this.loginDetails._id);
    this.token = localStorage.getItem("DEVICE_TOKEN");
    this.deviceUUID = localStorage.getItem("UUID");
  }
  ionViewDidLoad() {
    this.getProfile();
  }

  ngOnInit() { }

  getProfile() {
    this.apicall.startLoading().present();
    this.apicall.getprofileApi(this.loginDetails._id)
      .subscribe(data => {
        this.apicall.stopLoading();
        console.log("data of data: ", data)
        // this.profilesStudent = data[0].data;
        this.profilesParentname = data[0]._id.first_name;
        this.profilesParentphone = data[0]._id.phone;
        this.profilesParentemail = data[0]._id.email;
        this.profilesParenterelation = data[0]._id.relation
        this.profilesParent = data[0]._id;

        var i = 0, howManyTimes = data[0].data.length;
        let that = this;
        function f() {
          that.profilesStudent.push({
            "fname": data[0].data[i].students.name.fname ? data[0].data[i].students.name.fname : 'N/A',
            "schoolName": data[0].data[i].students.school.schoolName ? data[0].data[i].students.school.schoolName : 'N/A',
            "saddress": data[0].data[i].students.school.address ? data[0].data[i].students.school.address : 'N/A',
            "className": data[0].data[i].students.class.className ? data[0].data[i].students.class.className : 'N/A',
            "gender": data[0].data[i].students.gender ? data[0].data[i].students.gender : 'N/A',
            "bg": data[0].data[i].students.bg ? data[0].data[i].students.bg : 'N/A',
            "haddress": data[0].data[i].students.address ? data[0].data[i].students.address : 'N/A',
          })
          var _studId = data[0].data[i].students._id;
          that.profilesStudent[that.profilesStudent.length - 1].studmapId = "b" + i;
          that.allData.map = GoogleMaps.create(that.profilesStudent[that.profilesStudent.length - 1].studmapId, {
            camera: {
              target: { lat: data[0].data[i].students.latLng.lat, lng: data[0].data[i].students.latLng.lng },
              zoom: 13
            }
          });
          that.allData.map.addMarker({
            title: 'Student position',
            icon: 'green',
            animation: 'DROP',
            draggable: true,
            position: {
              lat: data[0].data[i].students.latLng.lat,
              lng: data[0].data[i].students.latLng.lng
            }
          })
            .then(marker => {
              marker.on(GoogleMapsEvent.MARKER_DRAG_END, that.profilesStudent[that.profilesStudent.length - 1].studmapId)
                .subscribe(() => {
                  that.markerlatlong = marker.getPosition();
                  Geocoder.geocode({
                    "position": {
                      lat: that.markerlatlong.lat,
                      lng: that.markerlatlong.lng
                    }
                  }).then((results: GeocoderResult[]) => {
                    if (results.length == 0) {
                      return null;
                    }

                    let address: any = [
                      results[0].subThoroughfare || "",
                      results[0].thoroughfare || "",
                      results[0].locality || "",
                      results[0].adminArea || "",
                      results[0].postalCode || "",
                      results[0].country || ""
                    ].join(", ");
                    that.addressofstudent = results[0].extra.lines[0];
                    that.sendAddress(that.addressofstudent, _studId, that.markerlatlong.lat, that.markerlatlong.lng);
                  });
                });
            });
          that.profilesStudent[that.profilesStudent.length - 1].mapid = "a" + i;
          that.allData.map = GoogleMaps.create(that.profilesStudent[that.profilesStudent.length - 1].mapid, {
            camera: {
              target: { lat: data[0].data[i].students.school.latLng.lat, lng: data[0].data[i].students.school.latLng.lng },
              zoom: 12
            }
          })

          that.allData.map.addMarker({
            position: {
              lat: data[0].data[i].students.school.latLng.lat,
              lng: data[0].data[i].students.school.latLng.lng
            }
          })

          i++;
          if (i < howManyTimes) {
            setTimeout(f, 200);
          }
        } f();
        console.log("profile data: ", that.profilesStudent)
        if (this.profilesStudent.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }
      }, error => {
        this.apicall.stopLoading();
        console.log(error);
      })
  }

  sendAddress(address, ID, Lat, Long) {
    var stdn = {
      "_id": ID,
      "latLng": { "lat": Lat, "lng": Long },
      "address": address
    }
    this.apicall.startLoading().present();
    this.apicall.studenEdit(stdn)
      .subscribe(data => {
        this.apicall.stopLoading();
        const toast = this.toastCtrl.create({
          message: 'Student location updated. !',
          duration: 3000,
          position: 'bottom'
        })
        toast.present();
        // this.getProfile();
      },
        error => {
          this.apicall.stopLoading();
          console.log(error);
        });
  }
}
