import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiveSingleDevice } from './live-single-device';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
import { CallNumber } from '@ionic-native/call-number';

@NgModule({
  declarations: [
    LiveSingleDevice,
  ],
  imports: [
    IonicPageModule.forChild(LiveSingleDevice),
    IonBottomDrawerModule
  ],
  providers: [CallNumber]
})
export class LiveSingleDeviceModule {}