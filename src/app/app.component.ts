import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, AlertController, App, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { ApiCallerProvider } from '../providers/api-caller/api-caller';
import { Uid } from '@ionic-native/uid';
import { Diagnostic } from '../../node_modules/@ionic-native/diagnostic';
import { Storage } from '@ionic/storage';
export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon: string;
}
@Component({
  selector: 'app-main-page',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  pages: PageInterface[] = [
    { title: 'Home', pageName: 'TabsPage', tabComponent: 'Tab1Page', index: 0, icon: 'home' },
    // { title: 'Tab 2', pageName: 'TabsPage', tabComponent: 'Tab2Page', index: 1, icon: 'contacts' },
    { title: 'Fee', pageName: 'FeePage', icon: 'cash' },
  ];
  rootPage: any;
  islogin: any = {};
  token: any;
  isDealer: any;
  notfiD: boolean;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    private push: Push,
    public alertCtrl: AlertController,
    public app: App,
    public apiCall: ApiCallerProvider,
    public toastCtrl: ToastController,
    private tts: TextToSpeech,
    private uid: Uid,
    private diagnostic: Diagnostic,
    public storage: Storage
  ) {

    platform.ready().then(() => {
      this.getPermission();
      statusBar.styleDefault();
      statusBar.hide();
      this.splashScreen.hide();

      platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();
        if (nav.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
          nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
        } else {
          platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
        }
        // }
      });
    });
    this.pushNotify();
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.initializeApp();

    if (localStorage.getItem("LOGGED_IN")) {
      this.rootPage = 'TabsPage';
    } else {
      this.rootPage = 'LoginPage';
    }

    this.events.subscribe('user:updated', (udata) => {
      this.islogin = udata;
      this.isDealer = udata.isDealer;
      console.log("islogin=> " + JSON.stringify(this.islogin));

    });

  }

  getPermission() {
    this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.READ_PHONE_STATE).then((status) => {
      if (status != this.diagnostic.permissionStatus.GRANTED) {
        this.diagnostic.requestRuntimePermission(this.diagnostic.permission.READ_PHONE_STATE).then((data) => {
          var imei = this.uid.IMEI;
        })
      }
      else {
        console.log("We have the permission");
        localStorage.setItem("IMEI", this.uid.IMEI);
        localStorage.setItem("UUID", this.uid.UUID);
      }
    }, (statusError) => {
      console.log(`statusError=> ` + statusError);
    });
  }

  pushNotify() {
    let that = this;
    that.push.hasPermission()                       // to check if we have permission
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
          that.pushSetup();
        } else {
          that.pushSetup();
          console.log('We do not have permission to send push notifications');
        }
      });
  }
  pushSetup() {
    // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
    this.push.createChannel({
      id: "bus_reached",
      description: "bus_reached description",
      sound: 'bus_reached',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('bus_reached Channel created'));

    this.push.createChannel({
      id: "student_deboarded",
      description: "student_deboarded description",
      sound: 'student_deboarded',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('student_deboarded Channel created'));

    this.push.createChannel({
      id: "student_onborded",
      description: "student_onborded description",
      sound: 'student_onborded',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('student_onborded Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('default Channel created'));

    // Delete a channel (Android O and above)
    // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))

    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        icon: 'safety365',
        iconColor: 'red'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {
      // if (localStorage.getItem("notifValue") != null) {
      //   if (localStorage.getItem("notifValue") == 'true') {
      this.tts.speak(notification.message)
        .then(() => console.log('Success'))
        .catch((reason: any) => console.log(reason));
      //   }
      // }
      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      } else {
        // this.nav.setRoot('AllNotificationsPage');
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        // alert(registration.registrationId)
        console.log("device token => " + registration.registrationId);
        // console.log("reg type=> " + registration.registrationType);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        // that.storage.set("DEVICE_TOKEN", registration.registrationId);

      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
      // alert('Error with Push plugin' + error)
    });
  }

  initializeApp() {
    let that = this;
    that.platform.ready().then(() => {
      that.pushNotify();
    });
  }

  openPage(page: PageInterface) {
    let params = {};

    // The index is equal to the order of our tabs inside tabs.ts
    if (page.index) {
      params = { tabIndex: page.index };
    }

    // The active child nav is our Tabs Navigation
    if (this.nav.getActiveChildNav() && page.index != undefined) {
      this.nav.getActiveChildNav().select(page.index);
    } else {
      // Tabs are not active, so reset the root page 
      // In this case: moving to or from SpecialPage
      this.nav.setRoot(page.pageName, params);
    }
  }

  isActive(page: PageInterface) {
    // Again the Tabs Navigation
    let childNav = this.nav.getActiveChildNav();

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    // Fallback needed when there is no active childnav (tabs not active)
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary';
    }
    return;
  }
}
