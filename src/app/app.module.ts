import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { IonBottomDrawerModule } from '../../node_modules/ion-bottom-drawer';
import { Push } from '@ionic-native/push';
import { Geolocation } from '@ionic-native/geolocation';
import { AppVersion } from '@ionic-native/app-version';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TextToSpeech } from '@ionic-native/text-to-speech';

// import { Pro } from '@ionic/pro';
// import { Injectable, Injector } from '@angular/core';
import { ApiCallerProvider } from '../providers/api-caller/api-caller';
// import { TabsPage } from '../pages/tabs/tabs';
import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';

import { Diagnostic } from '@ionic-native/diagnostic';
import { Uid } from '@ionic-native/uid';
import { IonicStorageModule } from '@ionic/storage';

// Pro.init('206faa80', {
//   appVersion: '2.1'
// })

// @Injectable()
// export class MyErrorHandler implements ErrorHandler {
//   ionicErrorHandler: IonicErrorHandler;

//   constructor(injector: Injector) {
//     try {
//       this.ionicErrorHandler = injector.get(IonicErrorHandler);
//     } catch (e) {
//       // Unable to get the IonicErrorHandler provider, ensure
//       // IonicErrorHandler has been added to the providers list below
//     }
//   }

//   handleError(err: any): void {
//     Pro.monitoring.handleNewError(err);
//     // Remove this if you want to disable Ionic's auto exception handling
//     // in development mode.
//     this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
//   }
// }

@NgModule({
  declarations: [
    MyApp,
    // TabsPage
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      // scrollPadding: false,
      // scrollAssist: false
    }),
    IonicStorageModule.forRoot(),
    IonBottomDrawerModule,
    SelectSearchableModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    IonicErrorHandler,
    // [{ provide: ErrorHandler, useClass: MyErrorHandler }],
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiCallerProvider,
    Network,
    ConnectivityServiceProvider,
    GoogleMaps,
    Geolocation,
    Spherical,
    Push,
    AppVersion,
    SocialSharing,
    TextToSpeech,
    Diagnostic,
    Uid,
  ]
})
export class AppModule { }
